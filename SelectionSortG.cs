﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_SelectionSort
{
    class SelectionSortG
    {
        public static void Sort<T>(T [] arr) where T:IComparable
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < arr.Length; j++)
                {
                        if (arr[j].CompareTo(arr[min]) < 0) min = j;
                    T temp = arr[i];
                    arr[i] = arr[min];
                    arr[min] = temp;
                }
            }
        }
    }
}
