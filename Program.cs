﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic_SelectionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] arr1 = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };

            Rectanguares[] arr1 = { new Rectanguares(1, 1), new Rectanguares(4, 4), new Rectanguares(2, 2) };
            SelectionSortG.Sort(arr1);

            for (int i = 0; i < arr1.Length; i++)
            {
                Console.WriteLine("{0 }", arr1[i]);
            }
        }

        class Rectanguares : IComparable
        {
            int x;
            int y;
            public Rectanguares(int a, int b)
            {
                x = a;
                y = b;
            }

            public int CompareTo(object obj)
            {
                if (((Rectanguares)obj).x + ((Rectanguares)obj).y > (x + y))
                {
                    return -1;
                }
                else if (((Rectanguares)obj).x + ((Rectanguares)obj).y == (x + y))
                {
                    return 0;
                }
                else return 1;
            }
            public override string ToString()
            {
                return "x: " + x + "y: " + y;
            }
        }
    }
}
